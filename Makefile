

PWD=${shell pwd}
UID=${shell id -u}
GID=${shell id -g}

# Docker command lines to run the structurizr, plantuml and sphix commands
#
STRUCTURIZR_IMAGE=amarburg/structurizr-cli-docker:latest
STRUCTURIZR=docker run --rm -v ${PWD}:/workdir  -u ${UID}:${GID} ${STRUCTURIZR_IMAGE}

PLANTUML=docker run -t --rm -u ${UID}:${GID} -v $(PWD):/workdir:rw dstockhammer/plantuml
GRAPHVIZ=docker run -t --rm -u ${UID}:${GID} -v $(PWD):/workdir:rw backplane/graphviz:latest  

SPHINX_IMAGE=amarburg/sphinx-myst-rtd:latest
SPHINX=docker run -t --rm -u ${UID}:${GID} -v $(PWD):/data --workdir /data ${SPHINX_IMAGE}

# Input file
SRC=diagrams/top_level.structurizr
PUML_DIR=tmp/puml
DOT_DIR=tmp/dot
PNG_DIR=_static/model
SPHINX_OUTPUT_DIR=public


# By default build everything
all: c4model html

c4model: structurizr dot

validate: diagrams/${SRC}
	${STRUCTURIZR} validate -workspace $<

# structurizer converts the .structurizr file to PlantUML .puml files
structurizr: ${SRC}
	${STRUCTURIZR} export -workspace /workdir/$< -format dot -o /workdir/${DOT_DIR}

plantuml: 
	${PLANTUML} -enablestats -verbose -output /workdir/${PNG_DIR} /workdir/${PUML_DIR}/*.puml

#   -Gnewrank=true
dot:
	for file in ${DOT_DIR}/*.dot ; do \
		${GRAPHVIZ} dot -v -Tpng -s200 -Epenwidth=5 -Gpenwidth=5 -Gnodesep=2.0 -Granksep=2.0 \
						-Gsplines=polyline -Glabeljust=l -Glabelloc=t -O /workdir/$${file}; \
	done
	mkdir -p ${PNG_DIR}
	mv ${DOT_DIR}/*.png ${PNG_DIR}

dot_help:
	${GRAPHVIZ} neato -?

# Alias "html" to "sphinx"
html:
	${SPHINX} -b html docs/ ${SPHINX_OUTPUT_DIR}

# Clean intermediate and final outputs
clean:
	rm -rf ${DOT_DIR} ${PNG_DIR} ${SPHINX_OUTPUT_DIR} 

${TMPDIR}:
	mkdir -p $@

${PNG_DIR}:
	mkdir -p $@


# Make rules related to building project-specific Docker images.
# Not required for day-to-day operation
#
docker:
	cd resources/structurizr-dockerfile && docker build -t ${STRUCTURIZR_IMAGE} .
	cd resources/sphinx-dockerfile && docker build -t ${SPHINX_IMAGE} .

docker_push:
	docker push ${STRUCTURIZR_IMAGE} 
	docker push ${SPHINX_IMAGE}



.PHONY: c4model structurizr plantuml clean html docker docker_push graphviz
