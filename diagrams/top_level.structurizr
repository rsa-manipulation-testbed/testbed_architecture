workspace "RSA Manipulation Testbed" "This is a model of the RSA manipulation testbed" {

    model {

        rovOperator = person "ROV Operator" "Nominal ROV operator"
        researcher = person "Researcher" "Grad student"

        testbedOperator = person "Testbed Operator" "Controls and configures the testbed"

        !include testbed.structurizr
        !include rov.structurizr

        externalPosition = softwareSystem "External positioning system" "(e.g., motion capture)" {
            -> navDrivers "Position estimate"
        }

        ##
        ## Interfaces between ROV and Testbed
        ## 

        forceTorqueSensor -> forceTorque "Force/torque info"

        bodyControl -> arm "Joint cmds" "/action/joint_cmd"
        arm -> bodyControl "/perception/encoders"

        bodyControl -> testbed "Gripper cmds"
        bodyControl -> rovControlInterface "ROV Thruster cmds" "/action/thruster_cmd"
        #bodyControl -> gripperDriver "/action/gripper_cmd"

        testbedSoftware -> perceptionDrivers "Position estimate (IK)"
        testbedPositionInterface -> navDrivers "/testbed/testbed_pos"

        tactileSensor -> tactileSensingDriver "Raw tactile data"


        sensors -> stereo "Camera data"
        sensors -> color "Camera data"


        ##
        ## Interfaces for People
        ##

        rovOperator -> rovSoftware "Intent"
        rovOperator -> ui "Intent"

        ui -> rovOperator ""
        #perceptionDrivers -> rovOperator "Raw data (e.g., video)"

        testbedOperator -> testbedSoftware "Configuration and direct drive commands"
        testbedOperator -> testbedControlInterface "Direct drive commands"

        researcher -> rovSoftware "Configures and updates"
        rovSoftware -> researcher "Monitors operation"

    }

    views {
        systemLandscape {
            include *
            autolayout lr
        }

        systemContext rovSoftware  {
            include *
        }

        container rovSoftware {
            include *
        }

        component depthMap {
            include *
        }

        component objId {
            include *
        } 

        component reconstruction {
            include *
        } 


        component perceptionDrivers {
            include *
        } 

        component graspSynthesis {
            include *
        }

        component pathPlanning {
            include *
        }

        component bodyControl {
            include *
        }

        component dss {
            include *
        }

        systemContext testbedSoftware  {
            include *
        }

        container testbedSoftware {
            include *
        }

        component testbedHardwareInterface {
            include *
        }

        component rovMotionEmulator {
            include *
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }

            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }

            relationship "Relationship" {
                ## Unfortunately doesn't do anything for PlantUML
                routing orthogonal   
            }
        }

        terminology {
            infrastructureNode hardwareSystem
        }
    }
    
}