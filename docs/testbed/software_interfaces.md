(testbed_interfaces)=

# Interfaces

The testbed is written to comply with the `ros_control` standard for `RobotHW`.    As such, the primary control interfaces are provided by controllers loaded from the `ros_controllers` package, rather than interfaces hardcoded into the `testbed_driver.`  The driver does expose internal state information as well as reconfigurable variables and services.

In the `ros_control` nomenclature, the testbed has **six** joints: `yaw`, `zaxis`, `corexy_x`, `corexy_y`, `corexy_a` and `corexy_b`.   The first two are directly controlled, while the middle two are the X- and Y-directions of the the CoreXY system.   They are mapped through a transmission to run two actuators: `corexy_a` and `corexy_b`.  A simple 1:1 transmission is used to map these actuators back to joint space so the standard `joint_state_controller` can publish their state, but those joints can't be controlled directly.

The system supports a number of different control modes for each of the joints, but for each mode is only a direct passthrough to a control loop in the actuators themselves.

The available controllers are described in [controller.yaml](https://gitlab.com/rsa-manipulation-testbed/testbed_driver/-/blob/main/config/controllers.yaml).  The actual controller in use for each axis can be set by modifying the launch file, or using the service calls to the `controller_manager`. 

## joint_state_controller

A `joint_state_controller` publishes the current state of all of the joints.

## Yaw 
Two controllers are defined for the yaw motor:

* `yaw_position_controller` is a `JointPositionController`.   It listens to a topic `command` (std_msgs::Float64) which is passed straight to the TMotor.
* `yaw_velocity_controller` is a `JointVelocityController`.  It listens to a topic `'command` (std_msgs::Float64) which is passed straight to the TMotor.

The control loop coefficients of the motors can be set by dynamic reconfigure.

## CoreXY

The CoreXY system is slightly more complicated in that it can control two axes (X and Y), but requires coordinated action from two actators (which we call A and B).

In software, the two TMotors are modelled as two `ros_control` "actuators": `corexy_a` and `corexy_b`.    Three "transmissions" are defined to map from the actuators to `ros_control` joints:

* The `CoreXYTransmission` is a differential transmission which encodes the dynamics of the CoreXY mechanism, and is the primary means of controlling the system.
* The `SimpleXYA` and `SimpleXYB` transmissions are simple 1:1 transmissions which map the current state of each actuator as a "joint" for `ros_control`.   Right now we don't define any mechanisms for sending commands directly to the actuator through this transmission.

Two controllers are defined for the CoreXYT:

* `corexy_position_controller` is a `JointGroupPositionController` and listens on a topic `command` (std_msgs::Float64MultiArray).  It maps the requested positions to the `corexy_a` and `corexy_b` motors and passes them directly to the TMotors.
* `corexy_velocity_controller` is a `JointGroupVelocityController` and listens on a topic `command` (std_msgs::Float64MultiArray).  It maps the requested velocity to the `corexy_a` and `corexy_b` motors and passes them directly to the TMotors.



The primary interfaces are managed by the `controller_manager` in [testbed_driver](https://gitlab.com/rsa-manipulation-testbed/testbed_driver).   The current [controller.yaml](https://gitlab.com/rsa-manipulation-testbed/testbed_driver/-/blob/main/config/controllers.yaml) loads two plugins:

* A `joint_trajectory_controller` offers both an **action** and **topic** interface for controlling all four axes of the testbed.
* A `joint_state_controllers` which publishes the current state of all four axes of the testbed.

## Z-axis

TBD...