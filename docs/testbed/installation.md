# Software Installation

The majority of the testbed control software is ROS-based, although there are some peripheral utilities which run outside of ROS for simplicity.   The ROS-based software driver speaks to the motors and interfaces with the [ROS control](http://wiki.ros.org/ros_control) stack to allow the testbed to be driven by tools like [MoveIt!](https://moveit.ros.org/).

The fille software archiecture is described in the {ref}`detailed software description <software_overview>`

The core Testbed software is stored in two Gitlab repos:

* [testbed_hw](https://gitlab.com/rsa-manipulation-testbed/testbed_hw) is concerned with driving the actual Testbed, including communicating with the Clearpath motors. 
* [testbed_sw](https://gitlab.com/rsa-manipulation-testbed/testbed_sw) contains configuration files which describe and configure the testbed.   Some configuration is specific to the hardware testbed (for example, which Clearpath motors drive which axes).  Other configuration files are used for Gazebo simulation, and some are used for for hardware and sim operation.

## Installation

### Install the ROS software

(these instructions assume you have at least a basic ROS install, C/C++ compilers, etc)

Get prerequisities:

```
apt install -y python3-vcstool
```

On a system running ROS Noetic, create a ROS workspace and checkout out `testbed_hw`:

```
mkdir -p testbed_ws/src
cd testbed_ws/src
git clone https://gitlab.com/rsa-manipulation-testbed/testbed_hw
```

Then checkout the other dependencies using the `.rosinstall` file found in that repo, then use `rosdep` to get any other dependencies

```
cd ..
vcs import testbed_hw/testbed_hw.rosinstall
rosdep install --ignore-src --from-paths . -y
```

Then build:

```
catkin build
```




### Install the Clearpath Device Driver

The Clearpath communication hub requires a special device driver.   This must be built and installed manually **any time the system kernel is updated.**   If the driver is **not** installed, the system hub will show up as a `/tty/devACMx` device.  If installed properly, the hub will show up as a `/dev/ttyXRUSBx` device.

A short script [`install_kernel_modules.sh`](https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/blob/main/clearpath_sc_ros/install_kernel_module.sh) is included in `testbed_hw/clearpath_sc_ros` which will automate this building process.  It can be run from a checkout of tha repo or:

```
wget -O - https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/raw/main/clearpath_sc_ros/install_kernel_module.sh | /bin/bash -
```


