# Testbed overview

(testbed_hardware)=

```{image} ../../images/testbed_image.jpg
:class: bg-primary
:width: 100%
:align: center
```

## Hardware

The Testbed has a total of four motors.

Additional informal information on the [testbed wiki](https://gitlab.com/rsa-manipulation-testbed/testbed_wiki)

## Software

The testbed software communicates with the testbed hardware.

```{image} ../../_static/model/structurizr-TestbedSoftware-TestbedHardwareinterface-Component.dot.png
:width: 100%
```

The interface is written using the [ros_control](http://wiki.ros.org/ros_control) infrastructure.  The [main node](https://gitlab.com/rsa-manipulation-testbed/testbed_driver) implements the `ros_control` `hardware_interace::RobotHW` and includes a `controller_manager`.

The `controller_manager` interface lets the system dynamically load different control algorithms.  I believe we will use the [joint_trajectory_controller](http://wiki.ros.org/joint_trajectory_controller?distro=noetic) controllers.

The end user may communicate with the testbed hardware interface directly (to control the testbed directly), or can communicate with the {ref}`ROV emulator <rov_emulator>`

