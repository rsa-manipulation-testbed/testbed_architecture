(rov_emulator)=

# ROV Emulator

The ROV motion emulator maps between commands to the "virtual" ROV from the research software and the {ref}`testbed's software drivers <testbed_software>`

```{image} ../../_static/model/structurizr-TestbedSoftware-ROVMotionEmulator-Component.dot.png
:width: 100%
```
## Interfaces

### In Interfaces

TBD...

### Out Interfaces

The system passes through the remapped messages it receives.   The expectation is that is will pass on `trajectory_msgs/JointTrajectory` **actions** (and **topics**?) to the `joint_trajectory_controller` in the {ref}`testbed software <testbed_software#in_interfaces>`.

## Components

### ROV Frame Translator

Translates between the vehicle-relative coordinate frame and the global frame of the testbed.

### ROV Motion Simulator

Implements ROV motion (inertia, etc.)

### Disturbance Simulator



