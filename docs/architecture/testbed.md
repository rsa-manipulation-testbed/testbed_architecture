(testbed)=

# Testbed

## System Context

At a high level, the TestbedSoftware presents an interface to the ROV software that emulates an ROV -- it accepts ROV-context motion commands, which may be actual thruster throttle commands or an effort mapped to the principal axes in the ROV frame. 

It then publishes a position estimate based on its own kinematics.
We assume this position estimate from the testbed is "perfect" and any sort of dithering/noise or fusion with other position references occurs within the ROV software.

```{image} ../../_static/model/structurizr-TestbedSoftware-SystemContext.dot.png
```

## Container Model

Here is the full set of containers within the testbed software.

```{image} ../../_static/model/structurizr-TestbedSoftware-Container.dot.png
:width: 100%
```

The `Testbed Control Interface` and `Position Info Interface` are generic placeholders for the interfaces in and out of the testbed.  They may not represent actual software, but may be an agreed interface (set of ROS topics and service calls, etc).

The {ref}`ROV Emulator <rov_emulator>` converts the ROV-frame motion commands to testbed motion.

The {ref}`Testbed Interface <testbed_software>` is the interface to the testbed hardware itself.

