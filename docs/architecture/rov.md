# ROV Control System


## System Context


```{image} ../_static/model/structurizr-ROVSoftware-SystemContext.dot.png
:width: 100%
```

## Container Model

```{image} ../_static/model/structurizr-ROVSoftware-Container.dot.png
:width: 100%
```
