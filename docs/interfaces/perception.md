(perception_interface)=
# Perception Processing

Post-processing nodes which produce higher-level perceptual products.   Receives raw data from the {ref}`ROS nodes which interface with perception sensors and produce "raw" data <perception_drivers_interface>`


The following blocks are "active research":

* {ref}`DepthMap generation <depthmap_generation>` -- or the equivalent point cloud generated from a single stereo snapshot.
* {ref}`Reconstruction <reconstruction>` of the world and/or objects using multiple frames of data and platform motion (SLAM).

The following blocks are to be a "minimum working examples" drawn from published Open Source packages:

* **Object identification:**  
* **Object segmentation:**   Segmentation of object pixels in image, depthmap, point cloud.
* 


----

(depthmap_generation)=
## DepthMap generation

The system may also include depth map / point cloud generation software running in parallel to those running on the sensor itself.


```{image} ../../_static/model/structurizr-ROVSoftware-Depthmappointcloudgeneration-Component.dot.png
:width: 100%
```


----
## Object Identification



```{image} ../../_static/model/structurizr-ROVSoftware-ObjectIdentificationSegmentation-Component.dot.png
:width: 100%
```


----
(reconstruction)=
## Reconstruction
