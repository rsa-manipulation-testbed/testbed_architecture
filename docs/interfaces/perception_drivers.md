(perception_drivers_interface)=

# Perception Drivers

The perception block includes ROS nodes which interfaces with perception sensors and produce "raw" data.   It feeds data to {ref}`post-processing nodes <perception_interface>` which produce higher-level perceptual products

Here is the component diagram for the perception drivers:


```{image} ../../_static/model/structurizr-ROVSoftware-PerceptionDrivers-Component.dot.png
:width: 100%
```
## Camera Subsystem

We assume testing will use the [Trisect](https://rsa-perception-sensor.gitlab.io/trisect-docs/) perception system.  It includes color "operator" camera and monochrome stereo cameras. It also runs a stereo matching algorithm on board, which we use as the "default" 3D map. Novel stereo mapping algorithms may also be developed as a research product as part of the {ref}`perception processing <perception_interface>`.

### Stereo (Monochrome) Imagery


### Color (Operator) Imagery


### Depth Data

(...)

----

## Sonar

(When available)

### Inputs


### Outputs

#### Raw Sonar Data

Description
: Sonar data in a standardized, binary, tabular format with metainformation (number of beams and ranges, range bin size, etc).

ROS Message Type(s)
: [acoustic_msgs/SonarImage.msg](https://github.com/apl-ocean-engineering/hydrographic_msgs/blob/main/acoustic_msgs/msg/SonarImage.msg)

#### Sonar Imagery

Description
: Sonar data converted to an image format for easy viewing.

ROS Message Type
: [sensor_msgs/Image](http://docs.ros.org/en/api/sensor_msgs/html/msg/Image.html)

----
## Tactile Sensing

----
## Force-Torque Sensing



