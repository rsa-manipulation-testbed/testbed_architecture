(planning_interface)=

# Grasp Synthesis


```{image} ../../_static/model/structurizr-ROVSoftware-GraspPlanningNode-Component.dot.png
```


# Path Planning and Active Reconstruction

```{image} ../../_static/model/structurizr-ROVSoftware-PathPlanningandTrajectoryOptimization-Component.dot.png
```
