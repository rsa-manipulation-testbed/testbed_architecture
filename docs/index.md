# RSA Manipulation Testbed Architecture and Documentation

This site contains documentation and system architecture diagrams for the RSA Task 2 Autonomous Manipulation Testbed.

The project has two major components: a suite of software and hardware research which collectively would run on an underwater robot (`ROV Software`) and a mechatronic `Testbed` which allows repeatable, controlled evaluation of that research.

Each of those components has its own section within the documentation:


```{toctree}
:maxdepth: 1
:caption: Testbed Operation

testbed/overview.md
testbed/installation.md
testbed/configuration.md
testbed/running.md
```


```{toctree}
:maxdepth: 1
:caption: Testbed Hardware

testbed/testbed_hardware.md
testbed/testbed_software.md
testbed/software_interfaces.md
testbed/rov_emulator.md
```



```{toctree}
:maxdepth: 1
:caption: Software Architecture

architecture/rov.md
architecture/testbed.md
```

<!-- ```{toctree}
:maxdepth: 1
:caption: Software Interfaces

interfaces/perception_drivers.md
interfaces/perception.md
interfaces/control.md
interfaces/dss.md
interfaces/planning.md
``` -->


----

# About this content

The architecture diagrams presented here are structured using the [C4Model](https://c4model.com/), and are stored in two formats: software diagrams are stored in the [Structurizr DSL](https://structurizr.com/), and are converted to graphics using a combination of the [structurizr CLI](https://github.com/structurizr/cli) and the [PlantUML](https://plantuml.com) renderer.

This website is written in [Markdown](https://www.markdownguide.org/) in the [Sphinx](https://www.sphinx-doc.org/en/master/) environment using the [Read the Docs](https://readthedocs.org/) theme.  

For further details on how to contribute to the documentation, see [Gitlab](https://gitlab.com/rsa-manipulation-testbed/testbed_architecture)

<!-- # Indices and tables

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search` -->
